###Scheme

![Scheme](public/images/scheme.png)


### Featuers:
- MyVideo - User can Create (upload) and Update own videos.
- Rating
- Comments
- Add video to favorite
- Related videos
- Create new thumbnails to own video or choose from existing
- Set Start Video or Start Channel for Home page
- Menu and Home page channels

CTAdmin can:
- create Website
- enadbled features
- create/update/delete channels
- create menu from channels
- create list of channles for home page

### Peculiar properties
Peculiar properties of the project is the Video, the Thumbnail and the Channel entities is composite entities: the part of the entity's data are stored in our local DB and other part in VMPro side.  

###I follow next principles:

- Controllers should be poor.
- I tries split all process to simple  CRUD operation (if possible).
- Entities **should be rich**. Entities should **have business logic**. Entities should take in **account structure (on the picture above) and relations**. 
- Only repositories know that some entities are composite. 
- I want that my project will be compatible with popular software like: doctrine orm, sonata admin and other bundles.
- My requirements to DB/orm: data should have necessary level of normalization. Data should be integrity (foreign keys). And query should be optimized or  avoiding unnecessary queries.  Configuration for DB structure should be transparent for developer (see like in Resource/doctrine/*.yml)

### differenses from Moto aproach

1) I don't want to implement extra domain layers now but my solution can be extend to implement it when it will be needs.

2) my entities also take in account the structure and relations.
 
In my solution it is nessesary for enities to  have properties for all relations which enity has.

So for example if we have relation oneToManu between website and video then we should add property $website to the video entity and property $videos in to the website entity.

(and as I use LAzy loading collection it does not lead that repository will load all data immediately)

the benefit of it is:

a) I can put more BL into entity. for example if an entity video has a method addComment then inside the method I can check that this  feature  is enabled. 

    class Video
    {
        function addComment(Comment $comment)
        {
            if (!$this->getWebsite()->getFeatures()->isCommentFeatureEnabled()) {
                throw new Exeption('the feature comment is disabled');
            }
        
            $this->comments[] = $comment;
        }
    }
    
b) it compatible with ORM (I can use orm relations map) and doctrine can use this entity directly (I don't need to create additional entityes and convert it beetween layrs)

c) if I need any property from parent I can always use it

d) I can use php object type in arguments of a method 

###Assumption
I didn't implement autorisation/authontification yet.
I didn't implemen API response yet. (aplicatin work like classic website for now)


###Code structure

Entities (models):
*src/Entity*

Doctrine mapping 
*src/Resources/config/doctrine/.yml*

Repositories. (I have created Repo only for composite model (video, channel, Thumbnail) because for other I can use Doctrine default repository
*src/Repository*

Lazy Load collection for relations (now I have for channels video and for preview of videos) for DB relation doctrine has own.
*src/Repository/Collection/*

VMPro client. I placed it inside repository dir because I assume that only Repository need it.
*src/Repository/VMProClient/*

Sonata Admin
*src/Admin

Controllers and DTO (in the example  I use twig templates that's why I don't need DTO but in API version we can use serialazer or DTO)
We need DTO (or serialazer) to convert Entity to api response and back if we need to have different structure for api then our entities have
*src/Controller
*src/Controller/DTO

*Service/DataReplication - service which replicate channels from VMPRO. (in develop still)



