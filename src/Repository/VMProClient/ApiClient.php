<?php

namespace App\Repository\VMProClient;

use App\Entity\Website\DataSource;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;

class ApiClient
{
    private $vmproUrl = 'https://api.video-cdn.net/v1/vms/';

    private $dataSourceId;

    private $vmproId;

    private $playerId;

    private $accessToken;

    private $client;

    public function __construct()
    {
        $this->client = new GuzzleClient(['base_uri' => $this->vmproUrl]);
    }

    public function connect(DataSource $dataSource): self
    {
        $dataSourceId = spl_object_id($dataSource);

        if ($this->dataSourceId == $dataSourceId) {
            return $this;
        }

        dump('connect');

        $this->dataSourceId = $dataSourceId;

        $this->vmproId = $dataSource->getVmproId();

        $this->playerId = $dataSource->getVmproPlayerId();

        $fields = [
            'username' => $dataSource->getVmproUser(),
            'password' => $dataSource->getVmproPassword(),
        ];

        $res = $this->client->post( 'auth/login', [RequestOptions::JSON => $fields]);

        $this->accessToken = json_decode($res->getBody()->getContents())->accessToken;

        return $this;
    }

    public function getVideosByChannelId(int $channelId, ?int $offset, ?int $limit): array
    {
        dump(['getVideosByChannelId', $channelId, $offset, $limit]);

        $querys = [
            'channel_id' => $channelId,
            'offset' => $offset,
            'limit' => $limit,
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/videos', [RequestOptions::HEADERS => $headers, RequestOptions::QUERY => $querys]);

        return json_decode($res->getBody()->getContents(), true);
    }

    public function getVideo(string $id): array
    {
        dump(['getVideo', $id]);

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/videos/'.$id, [RequestOptions::HEADERS => $headers]);

        return json_decode($res->getBody()->getContents(), true);
    }

    public function getChannels(): array
    {
        dump('getChannels');

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/channels', [RequestOptions::HEADERS => $headers]);

        return json_decode($res->getBody()->getContents(), true)['children'];
    }

    public function getVideoToken(string $videoId): ?string
    {
        dump(['getVideoToken', $videoId]);

        $querys = [
            'player_definition_id' => $this->playerId,
            'embed_type' => 'html',
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/videos/'.$videoId.'/embed-codes', [RequestOptions::HEADERS => $headers, RequestOptions::QUERY => $querys]);


        $result = json_decode($res->getBody()->getContents(), true);

        if (preg_match('/token="(.*?)"/', $result['embedCode'], $match)) {

           return $match[1];
        }

        return null;
    }

    public function createVideo(string $filename, string $name, ?string $description): ?string
    {
        $fields = [
            'fileName' => $filename,
            'title' => $name,
            'description' => $description,
            'autoPublish' => true
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $response = $this->client->post($this->vmproId.'/videos/', [RequestOptions::HEADERS => $headers,  RequestOptions::JSON => $fields]);

        if (isset($response->getHeader('location')[0])
            && preg_match('/\/videos\/(.*)/', $response->getHeader('location')[0], $match)) {

            return $match[1];
        }

        return null;
    }

    public function updateVideo(string $videoId, string $name, ?string $description)
    {
        $fields = [
            'title' => $name,
            'description' => $description
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->patch($this->vmproId.'/videos/'.$videoId, [RequestOptions::HEADERS => $headers,  RequestOptions::JSON => $fields]);

    }

    public function getVideoChannels(string $videoId): array
    {
        dump(['getVideoChannels', $videoId]);

        $fields = [
            'videoId' => $videoId
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/channels', [RequestOptions::HEADERS => $headers, RequestOptions::QUERY => $fields]);

        return json_decode($res->getBody()->getContents(), true)['children'];
    }

    public function addVideoToChannel(string $videoId, int $channelId)
    {
        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->post($this->vmproId.'/channels/'.$channelId.'/videos/'.$videoId, [RequestOptions::HEADERS => $headers]);
    }

    public function removeVideoFromChannel(string $videoId, int $channelId)
    {
        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->delete($this->vmproId.'/channels/'.$channelId.'/videos/'.$videoId, [RequestOptions::HEADERS => $headers]);
    }

    public function getVideoUploadUrl(string $videoId): ?string
    {
        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $response = $this->client->get($this->vmproId.'/videos/'.$videoId.'/url', [RequestOptions::HEADERS => $headers]);

        if (isset($response->getHeader('location')[0])) {
            return $response->getHeader('location')[0];
        }

        return null;
    }

    public function getVideosByIds(array $ids): array
    {
        if (empty($ids)) {
            return [];
        }

        $query_string = 'video_ids[]=' . implode('&video_ids[]=', $ids);

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $response = $this->client->get($this->vmproId.'/videos?' . $query_string, [RequestOptions::HEADERS => $headers]);

        return json_decode($response->getBody()->getContents(), true)['videos'];
    }

    public function getVideoThumbnails(string $videoId): array
    {
        dump(['getVideoThumbnails', $videoId]);

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $response = $this->client->get($this->vmproId.'/videos/'.$videoId.'/thumbnails', [RequestOptions::HEADERS => $headers]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function addThumbnailToVideo(string $videoId, int $thumbnailId)
    {
        $fields = [
            'active' => true
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->patch($this->vmproId.'/videos/'.$videoId.'/thumbnails/'.$thumbnailId, [RequestOptions::HEADERS => $headers,  RequestOptions::JSON => $fields]);
    }

    public function createVideoThumbnail(string $videoId, string $file)
    {
        $thumbnailId = null;
        $thumbnailUrl = null;

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        //create entity
        $response = $this->client->post($this->vmproId.'/videos/'.$videoId.'/thumbnails', [RequestOptions::HEADERS => $headers]);

        if (isset($response->getHeader('location')[0])
            && preg_match('/\/thumbnails\/(.*)/', $response->getHeader('location')[0], $match)) {

            $thumbnailId = $match[1];

            //get upload url
            $response = $this->client->get($this->vmproId.'/videos/'.$videoId.'/thumbnails/'.$thumbnailId.'/upload-url', [RequestOptions::HEADERS => $headers]);

            if (isset($response->getHeader('location')[0])) {

                $uploadUrl = $response->getHeader('location')[0];

                $multipart = [
                    [
                        'name'     => 'file',
                        'contents' => fopen($file, 'r')
                    ]
                 ];

                //upload file
                $this->client->post($uploadUrl, ['multipart'  => $multipart]);
            }
        }
    }

    public function getVideoKeywords(string $videoId): array
    {
        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $response = $this->client->get($this->vmproId.'/videos/'.$videoId.'/keywords', [RequestOptions::HEADERS => $headers]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function removeVideoKeyword(string $videoId, int $keywordId)
    {
        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->delete($this->vmproId.'/videos/'.$videoId.'/keywords/'.$keywordId, [RequestOptions::HEADERS => $headers]);
    }

    public function addVideoKeyword(string $videoId, string $keyword)
    {
        $fields = [
            'text' => trim($keyword)
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->post($this->vmproId.'/videos/'.$videoId.'/keywords', [RequestOptions::HEADERS => $headers, RequestOptions::JSON => $fields]);
    }

    public function updateVideoKeywords(string $videoId, array $keywords)
    {
        foreach ($this->getVideoKeywords($videoId) as $keyword) {
            $this->removeVideoKeyword($videoId, $keyword['id']);
        }

        foreach ($keywords as $keyword) {
            $this->addVideoKeyword($videoId, $keyword);
        }
    }

    public function getVideosByKeyword(string $keyword): array
    {
        $querys = [
            'search_term' => $keyword,
            'search_in_field' => 'keywords',
            'limit' => 5,
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $res = $this->client->get($this->vmproId.'/videos', [RequestOptions::HEADERS => $headers, RequestOptions::QUERY => $querys]);

        return json_decode($res->getBody()->getContents(), true)['videos'];
    }

    public function updateChannel(int $channelId, ?int $parentId, string $name, ?string $description)
    {
        dump(['updateChannel', $channelId]);

        $fields = [
            'parentId' => $parentId,
            'name' => $name,
            'description' => $description,
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->patch($this->vmproId.'/channels/'.$channelId, [RequestOptions::HEADERS => $headers, RequestOptions::JSON => $fields]);
    }

    public function createChannel(?int $parentId, string $name, ?string $description)
    {
        dump(['createChannel', $parentId, $name]);

        $fields = [
            'parentId' => $parentId,
            'name' => $name,
        ];

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->post($this->vmproId.'/channels', [RequestOptions::HEADERS => $headers, RequestOptions::JSON => $fields]);
    }

    public function removeChannel(int $channelId)
    {
        dump(['removeChannel', $channelId]);

        $headers = [
            'Authorization' => 'Bearer '.$this->accessToken
        ];

        $this->client->delete($this->vmproId.'/channels/'.$channelId, [RequestOptions::HEADERS => $headers]);
    }
}