<?php

namespace App\Repository;

use App\Entity\Thumbnail;
use App\Entity\Video;
use App\Repository\VMProClient\ApiClient;
use Doctrine\ORM\EntityManagerInterface;

class ThumbnailRepository
{
    private $apiClient;

    private $entityManager;

    public function __construct(ApiClient $apiClient, EntityManagerInterface $entityManager)
    {
        $this->apiClient = $apiClient;
        $this->entityManager = $entityManager;
    }

    private function buildThumbnailModel(?Thumbnail $thumbnail, ?array $thumbnailVmpro, Video $video): Thumbnail
    {
        if (!$thumbnail) {
            $thumbnail = new Thumbnail();
            $thumbnail->setVideo($video);
        }

        if (isset($thumbnailVmpro['id']))  {
            $thumbnail->setId($thumbnailVmpro['id']);
        }

        if (isset($thumbnailVmpro['items'][0]['url'])) {
            $thumbnail->setUrl($thumbnailVmpro['items'][0]['url']);
        }

        return $thumbnail;
    }

    public function findByVideo(Video $video): array
    {
        $thumbnails = [];

        foreach ($this->apiClient->connect($video->getWebsite()->getDataSource())->getVideoThumbnails($video->getExtId()) as $thumbnail) {
            $thumbnails[] = $this->buildThumbnailModel(null, $thumbnail, $video);
        }

        return $thumbnails;
    }

    public function save(Thumbnail $thumbnail): Thumbnail
    {
        $this->apiClient->connect($thumbnail->getVideo()->getWebsite()->getDataSource())->createVideoThumbnail($thumbnail->getVideo()->getExtId(), $thumbnail->getFileName());

        return $thumbnail;
    }
}