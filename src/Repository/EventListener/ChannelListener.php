<?php

namespace App\Repository\EventListener;

use App\Entity\Channel;
use App\Repository\VideoRepository;
use App\Repository\VMProClient\ApiClient;
use App\Repository\Collection\Channel\VideoCollection;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class ChannelListener
{
    private $apiClient;

    private $videoRepository;

    public function __construct(ApiClient $apiClient, VideoRepository $videoRepository)
    {
        $this->apiClient = $apiClient;
        $this->videoRepository = $videoRepository;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        if ($args->getObject() instanceof Channel) {
            $this->linkVideoCollection($args->getObject());
        }
    }

    public function linkVideoCollection(Channel $channel)
    {
        if ($channel) {
            $lazyVideoCollection = (new VideoCollection($channel))->setRepository($this->videoRepository);
            $channel->setVideos($lazyVideoCollection);
        }
    }
}