<?php

namespace App\Repository;

use App\Entity\Channel;
use App\Entity\Thumbnail;
use App\Entity\User;
use App\Entity\Video;
use App\Entity\Website;
use App\Repository\Collection\Video\ChannelCollection;
use App\Repository\Collection\Video\KeywordCollection;
use App\Repository\Collection\Video\ThumbnailCollection;
use App\Repository\Collection\Video\TokenProperty;
use App\Repository\VMProClient\ApiClient;
use Doctrine\ORM\EntityManagerInterface;

class VideoRepository
{
    private $apiClient;

    private $entityManager;

    private $thumbnailRepository;

    private $channelRepository;

    public function __construct(
        ApiClient $apiClient,
        EntityManagerInterface $entityManager,
        ChannelRepository $channelRepository,
        ThumbnailRepository $thumbnailRepository)
    {
        $this->apiClient = $apiClient;
        $this->entityManager = $entityManager;
        $this->thumbnailRepository = $thumbnailRepository;
        $this->channelRepository = $channelRepository;
    }

    private function buildVideoModel(?Video $video, ?array $videoVmpro, Website $website): Video
    {
        if (!$video) {
            $video = new Video();
            $video->setWebsite($website);
        }

        if (isset($videoVmpro['id']))  {
            $video->setExtId($videoVmpro['id']);
        }

        if (isset($videoVmpro['title'])) {
            $video->setName($videoVmpro['title']);
        }

        if (isset($videoVmpro['description'])) {
            $video->setDescription($videoVmpro['description']);
        }

        if (isset($videoVmpro['thumbnail'])) {
            $thumbnail = new Thumbnail();
            $thumbnail->setUrl($videoVmpro['thumbnail']);
            $video->setThumbnail($thumbnail);
        }

        $lazyThumbnails = (new ThumbnailCollection($video))->setRepository($this->thumbnailRepository);
        $video->setThumbnails($lazyThumbnails);

        $lazyKeywords = (new KeywordCollection($video))->setRepository($this);
        $video->setKeywords($lazyKeywords);

        $lazyChannels = (new ChannelCollection($video))->setRepository($this->channelRepository);
        $video->setChannels($lazyChannels);

        $lazyToken = (new TokenProperty($video))->setRepository($this);
        $video->setToken($lazyToken);

        return $video;
    }

    public function find(Website $website, string $id): ?Video
    {
        $videoVmpro = null;

        try {
            $videoVmpro = $this->apiClient->connect($website->getDataSource())->getVideo($id);
        } catch (\Exception $exception) {

        }

        /* @var Video $video */
        $video = $this->entityManager->getRepository(Video::class)->findOneBy(['website'=>$website,'extId'=>$id]);

        if (!$videoVmpro && !$video) {
            return null;
        }

        $video = $this->buildVideoModel($video, $videoVmpro, $website);

        return $video;
    }

    /**
     * @param string[] $extIds
     * @return Video[]
     */
    public function findByIds(Website $website, array $extIds): array
    {
        $videos = [];

        $vmproVideosHashMap = [];
        $videosHashMap = [];

        //extract from vmpro by ids
        foreach ($this->apiClient->connect($website->getDataSource())->getVideosByIds($extIds) as $vmproVideo) {
            $vmproVideosHashMap[$vmproVideo['id']] = $vmproVideo;
        }

        //extract from video db by ids
        foreach($this->entityManager->getRepository(Video::class)->findBy(['website' => $website, 'extId' => $extIds]) as $video) {
            $videosHashMap[$video->getExtId()] = $video;
        }

        foreach ($extIds as $extId) {
            if (isset($vmproVideosHashMap[$extId])) {
                $videos[] = $this->buildVideoModel($videosHashMap[$extId], $vmproVideosHashMap[$extId], $website);
            }
        }

        return $videos;
    }

    public function findByChannel(Channel $channel, ?int $limit = null, ?int $offset = null): array
    {
        $videos = [];

        $website = $channel->getWebsite();

        $videosVmpro = $this->apiClient->connect($website->getDataSource())->getVideosByChannelId($channel->getExtId(), $offset, $limit);

        foreach ($videosVmpro['videos'] as $videoVmpro) {
            $videos[] = $this->buildVideoModel(null, $videoVmpro, $website);
        }

        return $videos;
    }

    public function countByChannel(Channel $channel): int
    {
        $website = $channel->getWebsite();

        $videosVmpro = $this->apiClient->connect($website->getDataSource())->getVideosByChannelId($channel->getId(), 0, 0);

        return intval($videosVmpro['total']);
    }

    public function findByUser(User $user): array
    {
        $videoExtIds = [];

        foreach ($user->getVideos() as $video) {
            $videoExtIds[] = $video->getExtId();
        }

        return $this->findByIds($user->getWebsite(), $videoExtIds);
    }

    public function findFavoriteByUser(User $user): array
    {
        $videoExtIds = [];

        foreach ($user->getFavoriteVideos() as $favoriteVideo) {
            $videoExtIds[] = $favoriteVideo->getVideo()->getExtId();
        }

        return $this->findByIds($user->getWebsite(), $videoExtIds);
    }

    public function findRelatedVideos(Video $video): array
    {
        $videos = [];

        $keyword = $video->getKeywords()->first();

        if ($keyword) {
            foreach ($this->apiClient->connect($video->getWebsite()->getDataSource())->getVideosByKeyword($keyword) as $videoVmpro) {
                $videos[] = $this->buildVideoModel(null, $videoVmpro, $video->getWebsite());
            }
        }

        return $videos;
    }

    public function save(Video $video): Video
    {
        //create if video does not exist on vmpro
        if (!$video->getExtId()) {

            $videoVmproId = $this->apiClient->connect($video->getWebsite()->getDataSource())->createVideo(
                time().'.mp4',
                $video->getName(),
                $video->getDescription()
            );

            if ($videoVmproId) {
                $video->setExtId($videoVmproId);
            }
        }

        $apiConnect = $this->apiClient->connect($video->getWebsite()->getDataSource());

        //update video on vmpro side
        $apiConnect->updateVideo($video->getExtId(), $video->getName(), $video->getDescription());

        //update keywords
        $apiConnect->updateVideoKeywords($video->getExtId(), $video->getKeywords()->toArray());

        //update thumbnail
        if ($video->getThumbnail() && $video->getThumbnail()->getId()) {
            $apiConnect->addThumbnailToVideo($video->getExtId(), $video->getThumbnail()->getId());
        }

        //update channels
        $channelsToAdd = $video->getChannels()->map(function(Channel $item) { return $item->getExtId(); })->toArray();

        //remove old channels
        foreach ($apiConnect->getVideoChannels($video->getExtId()) as $channelVMPro) {
            if (in_array($channelVMPro['id'], $channelsToAdd)) {
                $key = array_search($channelVMPro['id'], $channelsToAdd);
                unset($channelsToAdd[$key]);
            } else {
                $apiConnect->removeVideoFromChannel($video->getExtId(), $channelVMPro['id']);
            }
        }

        foreach ($channelsToAdd as $channelId) {
            $apiConnect->addVideoToChannel($video->getExtId(), $channelId);
        }

        //create video in db if it does not exist
        if (!$video->getId()) {
            $this->entityManager->persist($video);
        }

        //update video in db
        $this->entityManager->flush();

        return $video;
    }

    public function getUploadUrl(Website $website, string $videoId): ?string
    {
        return $this->apiClient->connect($website->getDataSource())->getVideoUploadUrl($videoId);
    }

    public function getKeywords(Video $video): array
    {
        $keywords = $this->apiClient->connect($video->getWebsite()->getDataSource())->getVideoKeywords($video->getExtId());

        return array_map(function($keyword) { return $keyword['text']; }, $keywords);
    }

    public function getToken(Video $video): ?string
    {
        return $this->apiClient->connect($video->getWebsite()->getDataSource())->getVideoToken($video->getExtId());
    }
}