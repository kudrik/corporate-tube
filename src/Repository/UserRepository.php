<?php

namespace App\Repository;

use App\Entity\User;
use App\Entity\Website;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findFake(Website $website): User
    {
        return $this->findOneBy(['website' => $website, 'email' => 'fake@fake.ru']);
    }

    public function save(User $user)
    {
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush($user);
        $this->getEntityManager()->refresh($user);
    }
}