<?php

namespace App\Repository;

use App\Entity\Channel;
use App\Entity\Video;
use App\Repository\VMProClient\ApiClient;
use App\Service\DataReplication;
use Doctrine\ORM\EntityRepository;

class ChannelRepository extends EntityRepository
{
    private $apiClient;

    private $dataReplication;

    public function setApiClient(ApiClient $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    public function setDataReplication(DataReplication $dataReplication)
    {
        $this->dataReplication = $dataReplication;
    }

    public function findByVideo(Video $video): array
    {
        $channelsVmpro = $this->apiClient->connect($video->getWebsite()->getDataSource())->getVideoChannels($video->getExtId());

        $channelExtIds = array_map(function ($item) { return $item['id']; }, $channelsVmpro);

        $channels = $this->getEntityManager()->getRepository(Channel::class)->findBy(['extId' => $channelExtIds]);

        return $channels;
    }

    public function save(Channel $channel)
    {
        $parent = $channel->getParent();

        if ($channel->getExtId()) {
            //update on vmpro side
            $this->apiClient->connect($channel->getWebsite()->getDataSource())->updateChannel(
                $channel->getExtId(),
                $parent ? $parent->getExtId() : null,
                $channel->getName(),
                $channel->getDescription());
        } else {
            //create on vmproside
            $this->apiClient->connect($channel->getWebsite()->getDataSource())->createChannel(
                $parent ? $parent->getExtId() : null,
                $channel->getName(),
                $channel->getDescription());
        }

        if ($channel->getId()) {
            //update DB
            $this->getEntityManager()->merge($channel);
            $this->getEntityManager()->flush();
        } else {
            //create in DB
            //because VMPRo does not return channel id after creating then we sync all website channels
            $this->dataReplication->replicateChannels($channel->getWebsite());
        }
    }

    public function remove(Channel $channel)
    {
        try {
            $this->apiClient->connect($channel->getWebsite()->getDataSource())->removeChannel($channel->getExtId());
        } catch (\Exception $exception) {}
        $this->getEntityManager()->remove($channel);
        $this->getEntityManager()->flush();
    }
}