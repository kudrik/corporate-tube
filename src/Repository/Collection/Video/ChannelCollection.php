<?php

namespace App\Repository\Collection\Video;

use App\Entity\Video;
use App\Repository\ChannelRepository;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Lazy loading channels of the video
 *
 */
class ChannelCollection extends AbstractLazyCollection
{
    private $channelRepository;

    private $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function setRepository(ChannelRepository $channelRepository): self
    {
        $this->channelRepository = $channelRepository;

        return $this;
    }

    public function doInitialize()
    {
        $this->collection = new ArrayCollection($this->channelRepository->findByVideo($this->video));
    }
}