<?php

namespace App\Repository\Collection\Video;

use App\Entity\Video;
use App\Repository\VideoRepository;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\ArrayCollection;

class KeywordCollection extends AbstractLazyCollection
{
    private $video;

    private $videoRepository;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function setRepository(VideoRepository $videoRepository): self
    {
        $this->videoRepository = $videoRepository;

        return $this;
    }

    public function doInitialize()
    {
        $this->collection = new ArrayCollection($this->videoRepository->getKeywords($this->video));
    }
}