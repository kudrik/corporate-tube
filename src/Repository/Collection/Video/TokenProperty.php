<?php

namespace App\Repository\Collection\Video;

use App\Entity\Property\LazyProperty;
use App\Entity\Video;
use App\Repository\VideoRepository;


class TokenProperty extends LazyProperty
{
    private $videoRepository;

    private $video;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function setRepository(VideoRepository $videoRepository): self
    {
        $this->videoRepository = $videoRepository;

        return $this;
    }

    public function doInitialize()
    {
        $this->value = $this->videoRepository->getToken($this->video);
    }
}