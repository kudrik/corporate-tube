<?php

namespace App\Repository\Collection\Video;

use App\Entity\Video;
use App\Repository\ThumbnailRepository;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\ArrayCollection;

class ThumbnailCollection extends AbstractLazyCollection
{
    private $video;

    private $thumbnailRepository;

    public function __construct(Video $video)
    {
        $this->video = $video;
    }

    public function setRepository(ThumbnailRepository $thumbnailRepository): self
    {
        $this->thumbnailRepository = $thumbnailRepository;

        return $this;
    }

    public function doInitialize()
    {
        $this->collection = new ArrayCollection($this->thumbnailRepository->findByVideo($this->video));
    }
}