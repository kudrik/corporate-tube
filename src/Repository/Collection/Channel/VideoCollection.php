<?php

namespace App\Repository\Collection\Channel;

use App\Entity\Channel;
use App\Repository\VideoRepository;
use Doctrine\Common\Collections\AbstractLazyCollection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Lazy loading videos of the channel
 *
 */
class VideoCollection extends AbstractLazyCollection
{
    private $videoRepository;

    private $channel;

    public function __construct(Channel $channel)
    {
        $this->channel = $channel;
    }

    public function setRepository(VideoRepository $videoRepository): self
    {
        $this->videoRepository = $videoRepository;

        return $this;
    }

    public function first()
    {
        if ($videos = $this->videoRepository->findByChannel($this->channel, 1, 0)) {
            return reset($videos);
        }

        return null;
    }

    public function slice($offset, $length = null)
    {
        return $this->collection = new ArrayCollection($this->videoRepository->findByChannel($this->channel, $length, $offset));
    }

    public function count()
    {
        return $this->videoRepository->countByChannel($this->channel);
    }

    public function doInitialize()
    {
        $this->collection = new ArrayCollection($this->videoRepository->findByChannel($this->channel));
    }
}