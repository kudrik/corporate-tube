<?php

namespace App\Admin;

use App\Entity\User;
use App\Entity\Website;
use App\Entity\Website\HomeChannel;
use App\Entity\Website\Menu;
use App\Service\DataReplication;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class WebsiteAdmin extends WebsiteCommonAdmin
{
    /** @var DataReplication */
    private $dataReplication;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $website = $this->getSubject();

        $formMapper->add('name', TextType::class);
        $formMapper->add('host', TextType::class);
        $formMapper->end();
        $formMapper->with('VMPro settings');
        $formMapper->add('dataSource.vmproUser', TextType::class);
        if ($website->getId()) {
            $formMapper->add('vmproPasswordPlain', TextType::class, ['mapped'=>false, 'required' => false]);
        } else {
            $formMapper->add('dataSource.vmproPassword', TextType::class);
        }
        $formMapper->add('dataSource.vmproId', IntegerType::class);
        $formMapper->add('dataSource.vmproPlayerId', TextType::class);
        $formMapper->end();
        $formMapper->with('Identity Providers');
        $formMapper->add('identityProvider.idpHint', TextType::class, ['required' => false]);
        $formMapper->add('identityProvider.authRealm', TextType::class, ['required' => false]);
        $formMapper->add('identityProvider.authResource', TextType::class, ['required' => false]);
        $formMapper->add('identityProvider.authUrl', TextType::class, ['required' => false]);
        $formMapper->end();
        $formMapper->with('Features');
        $formMapper->add('feature.upload', CheckboxType::class, ['required' => false]);
        $formMapper->add('feature.comment', CheckboxType::class, ['required' => false]);
        $formMapper->add('feature.rating', CheckboxType::class, ['required' => false]);
        $formMapper->add('feature.relatedVideos', CheckboxType::class, ['required' => false]);
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
        $datagridMapper->add('host');
        $datagridMapper->add('feature.upload');
        $datagridMapper->add('feature.comment');
        $datagridMapper->add('feature.rating');
        $datagridMapper->add('feature.relatedVideos');
        $datagridMapper->add('dataSource.vmproUser');
        $datagridMapper->add('dataSource.vmproId');
        $datagridMapper->add('dataSource.vmproPlayerId');
        $datagridMapper->add('identityProvider.idpHint');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('host');
        $listMapper->addIdentifier('feature.upload');
        $listMapper->addIdentifier('feature.comment');
        $listMapper->addIdentifier('feature.rating');
        $listMapper->addIdentifier('feature.relatedVideos');
        $listMapper->add('_channels', 'actions', [
            'actions' => [
                'channels' => ['template' => 'admin/website_channels.html.twig']
            ]
        ]);
        $listMapper->add('_menu', 'actions', [
            'actions' => [
                'menu' => ['template' => 'admin/website_menu.html.twig'],
            ]
        ]);
        $listMapper->add('_home_channels', 'actions', [
            'actions' => [
                'homeChannels' => ['template' => 'admin/website_home_channels.html.twig'],
            ]
        ]);
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('Website')
                ->add('name')
                ->add('host')
            ->end()
            ->with('VMPro settings')
                ->add('dataSource.vmproUser')
                ->add('dataSource.vmproId')
                ->add('dataSource.vmproPlayerId')
            ->end()
            ->with('Identity Providers')
                ->add('identityProvider.idpHint')
                ->add('identityProvider.authRealm')
                ->add('identityProvider.authResource')
                ->add('identityProvider.authUrl')
            ->end()
            ->with('Features')
                ->add('feature.upload')
                ->add('feature.comment')
                ->add('feature.rating')
                ->add('feature.relatedVideos')
            ->end();
    }

    /**
     * @param Website $website
     */
    public function preUpdate($website)
    {
        if ($vmproPasswordPlain = $this->getForm()->get('vmproPasswordPlain')->getData()) {
            $website->getDataSource()->setVmproPassword($vmproPasswordPlain);
        }
    }

    public function setDataReplication(DataReplication $dataReplication)
    {
        $this->dataReplication = $dataReplication;
    }

    public function postUpdate($website)
    {
        $this->dataReplication->replicateChannels($website);
    }

    /**
     * @param Website $website
     */
    public function postPersist($website)
    {
        $website = $this->dataReplication->replicateChannels($website);

        $position = 0;

        foreach ($website->getRootChannels() as $channel) {

            $menu = new Menu();
            $menu->setChannel($channel);
            $menu->setPosition($position);
            $menu->setType(Menu::TYPE_CHANNEL);
            $website->addMenu($menu);

            $homeChannel = new HomeChannel();
            $homeChannel->setChannel($channel);
            $homeChannel->setPosition($position);
            $website->addHomeChannel($homeChannel);

            $position = $position + 10;
        }

        //@TODO temporary create fake user
        $fakeUser = new User();
        $fakeUser->setWebsite($website);
        $fakeUser->setEmail('fake@fake.ru');

        $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->persist($fakeUser);

        $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->flush();
    }
}