<?php

namespace App\Admin;

use App\Entity\Channel;
use App\Entity\Website;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChannelAdmin extends WebsiteChildCommonAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'lft',
    ];

    protected function configureBatchActions($actions)
    {
        unset($actions['delete']);
        return $actions;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Channel $channel */
        $channel = $this->getSubject();

        $formMapper->with('Channel');
        $formMapper->add(
            'parent',
            EntityType::class,
            [
                'class' => Channel::class,
                'choice_label' => 'getNameWithLevel',
                'required' => false,
                'disabled' => boolval($channel->getid()),
                'choices' => $channel->getWebsite()->getChannels(),
            ]
        );
        $formMapper->add('name', TextType::class);
        $formMapper->add('description', TextareaType::class, ['required' => false]);
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('getNameWithLevel', TextType::class, ['label' => 'Channel name', 'identifier' => true,  'header_style' => 'width: 25%']);
        $listMapper->add('description', TextType::class, ['collapse' => true]);
    }

    public function update($object)
    {
        $repo = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Channel::class);
        $repo->save($object);

        return $object;
    }

    public function delete($object)
    {
        $repo = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Channel::class);
        $repo->remove($object);
    }

    public function create($object)
    {
        $repo = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Channel::class);
        $repo->save($object);
    }
}