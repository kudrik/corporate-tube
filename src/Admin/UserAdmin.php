<?php

namespace App\Admin;

use App\Entity\User;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class UserAdmin extends WebsiteChildCommonAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('email', EmailType::class);

        $user = $this->getSubject();

        $formMapper->add('roles', ChoiceType::class,  [
            'choices'  => array_combine(User::ROLES, User::ROLES),
            'expanded' => false,
            'multiple' => true,
        ]);

        $formMapper->add('blocked', CheckboxType::class, ['required' => false]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('email');
        $datagridMapper->add(
            'roles',
            null,
            [],
            ChoiceType::class,
            ['choices' => array_combine(User::ROLES, User::ROLES)]);
        $datagridMapper->add('blocked');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('email');
        $listMapper->addIdentifier('roles', 'array', ['code'=> 'dsd']);
        $listMapper->addIdentifier('blocked', null, ['inverse'=> true]);
        $listMapper->add('getVideosCount');
        $listMapper->add('getCommentsCount');
    }
}