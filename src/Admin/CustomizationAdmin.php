<?php

namespace App\Admin;

use App\Entity\Channel;
use App\Entity\Website;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CustomizationAdmin extends WebsiteCommonAdmin
{
    protected $baseRouteName = 'websitecustomization';
    protected $baseRoutePattern = 'websitecustomization';

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Website $website */
        $website = $this->getSubject();

        $formMapper->add('name', TextType::class, ['disabled' => true]);
        $formMapper->end();
        $formMapper->with('Home page');
            if ($website->getSourceStartVideo()) {
                $formMapper->add(
                    'SourceStartVideo',
                    TextType::class,
                    [
                        'mapped' => false,
                        'required' => false,
                        'disabled' => true,
                        'data' => $website->getSourceStartVideo()->getExtId()
                    ]);
            }

            $formMapper->add(
                'sourceStartChannel',
                EntityType::class,
                [
                    'class' => Channel::class,
                    'choice_label' => 'name',
                    'choices' => $website->getChannels(),
                    'required' => false,
                ]);
        $formMapper->end();
        $formMapper->with('Styles');
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('sourceStartChannel.name');
        $listMapper->addIdentifier('sourceStartVideo.extId');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
        $collection->remove('create');
        $collection->remove('delete');
    }
}