<?php

namespace App\Admin;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;

class WebsiteCommonAdmin extends AbstractAdmin
{
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this->isChild() ? $this->getParent() : $this;

        $id = $admin->getRequest()->get('id');

        $website = $this->getObject($id);

        if ($id) {

            /** @var MenuItemInterface $menu */
            $menu = $this->getConfigurationPool()->getContainer()->get('sonata.admin.sidebar_menu');

            $websiteMenu = $menu->addChild($website->getName());

            if ($this->isGranted('ROLE_APP\ADMIN\WEBSITEADMIN_EDIT')) {
                $websiteMenu->addChild('Settings', [
                    'route' => 'admin_app_website_edit',
                    'routeParameters' => ['id' => $id]
                ]);
            } else {
                $websiteMenu->addChild('Settings', [
                    'route' => 'admin_app_website_show',
                    'routeParameters' => ['id' => $id]
                ]);
            }

            $websiteMenu->addChild('Channels', [
                'route' => 'admin_app_website_channel_list',
                'routeParameters' => ['id' => $id]
            ]);

            $websiteMenu->addChild('Pages', [
                'route' => 'admin_app_website_page_list',
                'routeParameters' => ['id' => $id]
            ]);

            $websiteMenu->addChild('Menu', [
                'route' => 'admin_app_website_website_menu_list',
                'routeParameters' => ['id' => $id]
            ]);

            $websiteMenu->addChild('Home channels', [
                'route' => 'admin_app_website_website_homechannel_list',
                'routeParameters' => ['id' => $id]
            ]);

            $websiteMenu->addChild('Customization', [
                'route' => 'websitecustomization_edit',
                'routeParameters' => ['id' => $id]
            ]);

            $websiteMenu->addChild('Users', [
                'route' => 'admin_app_website_user_list',
                'routeParameters' => ['id' => $id]
            ]);
        }
    }

    public function createQuery($context = 'list')
    {
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = parent::createQuery($context);

        $container = $this->getConfigurationPool()->getContainer();

        if (!$container->get('security.authorization_checker')->isGranted(['ROLE_SUPER_ADMIN'])) {

            $website = $container->get('security.token_storage')->getToken()->getUser()->getWebsite();

            $query->andWhere(
                $query->expr()->eq($query->getRootAliases()[0] . '.id', $website->getId())
            );
        }

        return $query;
    }
}