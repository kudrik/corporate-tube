<?php

namespace App\Admin;

use App\Entity\Channel;
use App\Entity\Website\HomeChannel;
use App\Entity\Website;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class HomeChannelAdmin extends WebsiteChildCommonAdmin
{
    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    );

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var HomeChannel $homeChannel */
        $homeChannel = $this->getSubject();

        $website = $homeChannel->getWebsite();

        if (!$homeChannel->getId()) {
            $homeChannel->setPosition(
                $homeChannel->getWebsite()->getHomeChannels()->count() * 10 + 10
            );
        }

        //exclude from channel selector channels which are already in menu
        $channels = $website->getChannels()->toArray();

        $homeChannels = $website->getHomeChannels();

        foreach ($channels as $i => $channel) {
            foreach ($homeChannels as $homeChannelItem) {
                if ($homeChannel->getChannel() && $homeChannel->getChannel()->getId() === $homeChannelItem->getId()) {
                    continue;
                }
                if ($homeChannelItem->getId() === $channel->getid()) {
                    unset($channels[$i]);
                    break;
                }
            }
        }

        unset($homeChannels);

        $formMapper->with('Home channel');
        $formMapper->add(
            'channel',
            EntityType::class,
            [
                'class' => Channel::class,
                'choice_label' => 'name',
                'choices' => $channels,
            ]
        );
        $formMapper->add('position', IntegerType::class, ['required' => false]);
        $formMapper->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    public function createSortableAdminQuery()
    {
        return $this->createQuery()->andWhere('o.website = :website')->setParameter('website', $this->getSubject()->getWebsite());
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('channel.name');
        $listMapper->add('_action', null, [
            'actions' => [
                'move' => [
                    'template' => 'admin/_sort.html.twig',
                ],
            ]
        ]);
    }
}