<?php

namespace App\Admin\Sortable;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SortableAdminController extends CRUDController
{
    public function moveAction($position)
    {
        $translator = $this->get('translator');

        if (!$this->admin->isGranted('EDIT')) {
            $this->addFlash(
                'sonata_flash_error',
                $translator->trans('flash_error_no_rights_update_position')
            );

            return new RedirectResponse($this->admin->generateUrl(
                'list',
                array('filter' => $this->admin->getFilterParameters())
            ));
        }

        $object = $this->admin->getSubject();

        if ($position == 'up') {
            $order = 'desc';
            $sign = '<';
        } elseif ($position == 'down') {
            $order = 'asc';
            $sign = '>';
        }
        /* @var $query \Doctrine\ORM\QueryBuilder */
        $query = $this->admin->createSortableAdminQuery();
        $query->andWhere('o.id <> :id');
        $query->andWhere('o.position ' . $sign . ' :position');
        $query->addOrderBy('o.position', $order);
        $query->setParameter('id', $object->getId());
        $query->setParameter('position', $object->getPosition());

        $neighbor = $query->setMaxResults(1)->getQuery()->getOneOrNullResult();

        if ($neighbor) {
            //@TODO write direct update (this query too heavy)
            $forObject = $neighbor->getPosition();
            $forNeighbor = $object->getPosition();

            $neighbor->setPosition(999998);
            $object->setPosition(999999);
            $this->admin->update($object);
            $this->admin->update($neighbor);

            $object->setPosition($forObject);
            $neighbor->setPosition($forNeighbor);
            $this->admin->update($object);
            $this->admin->update($neighbor);
        }

        if ($this->isXmlHttpRequest()) {
            return $this->renderJson(array(
                'result' => 'ok',
                'objectId' => $this->admin->getNormalizedIdentifier($object)
            ));
        }

        $this->addFlash(
            'sonata_flash_success',
            $translator->trans('flash_success_position_updated')
        );

        return new RedirectResponse($this->admin->generateUrl(
            'list',
            array('filter' => $this->admin->getFilterParameters())
        ));
    }
}