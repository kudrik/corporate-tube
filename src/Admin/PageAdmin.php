<?php

namespace App\Admin;

use App\Entity\Page;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class PageAdmin extends WebsiteChildCommonAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->with('Content');
        $formMapper->add('name', TextType::class);
        $formMapper->add('description', TextareaType::class, ['required' => false, 'attr' => ['class' => 'mceAdvanced']]);
        $formMapper->end();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
    }
}