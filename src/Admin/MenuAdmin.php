<?php

namespace App\Admin;

use App\Entity\Channel;
use App\Entity\Page;
use App\Entity\Website\Menu;
use App\Entity\Website;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class MenuAdmin extends WebsiteChildCommonAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Menu $menu */
        $menu = $this->getSubject();

        $website = $menu->getWebsite();

        $menuItems = $website->getMenu();

        if (!$menu->getId()) {
            $menu->setPosition(
                $menu->getWebsite()->getMenu()->count() * 10 + 10
            );
            $menu->setType(Menu::TYPE_CHANNEL);
        }

        //exclude from channel selector channels which are already in menu
        $channelsWithoutMenu = $website->getChannels()->toArray();

        foreach ($channelsWithoutMenu as $i => $channel) {
            foreach ($menuItems as $menuItem) {
                if ($menu && $menu->getId() === $menuItem->getId()) {
                    continue;
                }
                $channelInMenu = $menuItem->getChannel();
                if ($channelInMenu && $channel->getId() === $channelInMenu->getid()) {
                    unset($channelsWithoutMenu[$i]);
                    break;
                }
            }
        }

        //exclude from page selector pages which are already in menu
        $pagesWithoutMenu = $website->getPages()->toArray();

        foreach ($pagesWithoutMenu as $i => $page) {
            foreach ($menuItems as $menuItem) {
                if ($menu && $menu->getId() === $menuItem->getId()) {
                    continue;
                }
                $pageInMenu = $menuItem->getPage();
                if ($pageInMenu && $page->getId() === $pageInMenu->getid()) {
                    unset($pagesWithoutMenu[$i]);
                    break;
                }
            }
        }

        $typeChoises = [];

        if (count($channelsWithoutMenu)) {
            $typeChoises['Channel'] = Menu::TYPE_CHANNEL;
        }

        if (count($pagesWithoutMenu)) {
            $typeChoises['Page'] = Menu::TYPE_PAGE;
        }

        $typeChoises['Link'] = Menu::TYPE_LINK;

        $formMapper->with('Menu');
        $formMapper->add('type', ChoiceFieldMaskType::class, [
            'choices' => $typeChoises,
            'expanded' => true,
            'map' => [
                Menu::TYPE_CHANNEL => ['channel'],
                Menu::TYPE_PAGE => ['page'],
                Menu::TYPE_LINK => ['name','url']
            ],
            'required' => true,
            'attr' => ['placeholder' => 'Select']
        ]);
        $formMapper->add(
            'page',
            EntityType::class,
            [
                'class' => Page::class,
                'choice_label' => 'name',
                'choices' => $pagesWithoutMenu,
            ]
        );
        $formMapper->add(
            'channel',
            EntityType::class,
            [
                'class' => Channel::class,
                'choice_label' => 'name',
                'choices' => $channelsWithoutMenu,
            ]
        );
        $formMapper->add('name', TextType::class, ['required' => false]);
        $formMapper->add('url', TextType::class, ['required' => false]);
        $formMapper->add('position', IntegerType::class, ['required' => false]);
        $formMapper->end();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('move', $this->getRouterIdParameter().'/move/{position}');
    }

    public function createSortableAdminQuery()
    {
        return $this->createQuery()->andWhere('o.website = :website')->setParameter('website', $this->getSubject()->getWebsite());
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name');
        $listMapper->addIdentifier('type');
        $listMapper->add('_action', null, [
                'actions' => [
                    'move' => [
                        'template' => 'admin/_sort.html.twig',
                    ],
                ]
            ]);
    }
}