<?php

namespace App\Command;

use App\Entity\Website;
use App\Service\DataReplication;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DataReplicationCommand extends Command
{
    protected static $defaultName = 'app:data-replicate';

    private $dataReplication;

    private $entityManager;

    public function __construct(DataReplication $dataReplication, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->dataReplication = $dataReplication;

        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Replicate data from VMPRO');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->entityManager->getRepository(Website::class)->findAll() as $website) {

            $this->dataReplication->replicateChannels($website);
        }
    }
}