<?php

namespace App\Controller;

use App\Entity\Channel;
use App\Entity\Thumbnail;
use App\Entity\User;
use App\Entity\Video;
use App\Entity\Website;
use App\Repository\ThumbnailRepository;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class VideoUserController extends AbstractController
{
    private $videoRepository;

    private $thumbnailRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository, ThumbnailRepository $thumbnailRepository,  UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->thumbnailRepository = $thumbnailRepository;
        $this->userRepository = $userRepository;
    }

    //build form using simfony build, this does not needs in REST API version
    private function form(Video $video, Request $request)
    {
        $website = $video->getWebsite();

        $formBuilder = $this->createFormBuilder($video);
        $formBuilder->add('name', \Symfony\Component\Form\Extension\Core\Type\TextType::class);

        $formBuilder->add(
            'channels',
             \Symfony\Bridge\Doctrine\Form\Type\EntityType::class,
            [
                'class' => Channel::class,
                'multiple' => true,
                'choice_label' => 'name',
                'choices' => $website->getChannels(),
            ]
        );

        $formBuilder->add('description', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, ['required' => false]);
        $formBuilder->add('keywords', \Symfony\Component\Form\Extension\Core\Type\TextareaType::class, ['required' => false, 'mapped' => false, 'data' => implode(', ', $video->getKeywords()->toArray()) ]);

        if ($video && $video->getExtId()) { $formBuilder->add('thumbnailFile', \Symfony\Component\Form\Extension\Core\Type\FileType::class, ['mapped' => false,'required' => false]); }

        $formBuilder->add('save', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' => $video->getId() ? 'Save' : 'Create Video']);

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        return $form;
    }

    public function list(Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $user = $this->userRepository->findFake($website);

        $videos = $this->videoRepository->findByUser($user);

        return $this->render('user/myvideo.html.twig', ['website' => $website, 'videos' => $videos, 'menu' => $website->getMenu()]);
    }

    public function create(Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        if (!$website->getFeature()->isUpload()) {
            throw new \Exception('Upload feature is disabled');
        }

        $video = new Video();
        $video->setWebsite($website);

        $form = $this->form($video, $request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var Video $video */
            $video = $form->getData();
            $video->setWebsite($website);
            $video->setKeywords(
                new ArrayCollection(
                    explode(',', $form->get('keywords')->getData())
                )
            );
            $video->setUser($this->userRepository->findFake($website));

            $this->videoRepository->save($video);

            return $this->redirectToRoute('videoUpload', ['extId'=>$video->getExtId()]);
        }

        return $this->render('video/form.html.twig', ['website' => $website, 'form' => $form->createView(), 'menu' => $website->getMenu()]);
    }

    public function upload(string $extId, Request $request)
    {
        /* @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $url = $this->videoRepository->getUploadUrl($website, $extId);

        //build form using simfony build, this does not needs in REST API version
        $form = $this->createFormBuilder()
            ->setAction($url)
            ->add('file', \Symfony\Component\Form\Extension\Core\Type\FileType::class)
            ->add('save', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' => 'Create Video'])
            ->getForm();

        return $this->render('video/form.html.twig', ['website' => $website, 'form' => $form->createView()]);
    }

    public function update(string $extId, Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $form = $this->form($video, $request);

        if ($form->isSubmitted()) {

            /** @var Video $video */
            $video = $form->getData();
            $video->setWebsite($website);

            $extraData = $form->getExtraData();

            if ($form->get('thumbnailFile')->getData()) {
                $thumbnail = new Thumbnail();
                $thumbnail->setVideo($video);
                $thumbnail->setFileName($form->get('thumbnailFile')->getData());
                $this->thumbnailRepository->save($thumbnail);
            } elseif (isset($extraData['thumbnailId'])) {
                $thumbnail = new Thumbnail();
                $thumbnail->setId($extraData['thumbnailId']);
                $video->setThumbnail($thumbnail);
            }

            $video->setKeywords(
                new ArrayCollection(
                    explode(',', $form->get('keywords')->getData())
                )
            );

            $this->videoRepository->save($video);

            return $this->redirectToRoute('videoUpdate', ['extId'=>$video->getExtId()]);
        }

        return $this->render('video/form.html.twig', ['website' => $website, 'form' => $form->createView(), 'video' => $video, 'menu' => $website->getMenu()]);
    }
}