<?php

namespace App\Controller;

use App\Entity\Website;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class VideoController extends AbstractController
{
    private $videoRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository, UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->userRepository = $userRepository;
    }

    public function index(string $extId, Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $user = $this->userRepository->findFake($website);

        $isVideoFavorite = $user->isVideoFavorite($video);

        $relatedVideos = [];
        if ($website->getFeature()->isRelatedVideos()) {
            $relatedVideos = $this->videoRepository->findRelatedVideos($video);
        }

        return $this->render('video/index.html.twig', [
            'video' => $video,
            'relatedVideos' => $relatedVideos,
            'website' => $website,
            'menu' => $website->getMenu(),
            'isFavorite' => $isVideoFavorite
            ]
        );
    }

    public function setStartWebsite(string $extId, Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $website->setSourceStartVideo($video);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('video', ['extId' => $video->getExtId()]);
    }

    public function resetStartWebsite(string $extId, Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $website->setSourceStartVideo(null);

        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('video', ['extId' => $extId]);
    }
}