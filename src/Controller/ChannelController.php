<?php

namespace App\Controller;

use App\Entity\Channel;
use App\Entity\Website;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ChannelController extends AbstractController
{
    public function index(int $id, Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $channel = $this->getDoctrine()->getRepository(Channel::class)->findOneBy(['website' => $website, 'extId' => $id]);

        $videos = $channel->getVideos()->slice(0, 10);

        return $this->render('channel/index.html.twig', ['channel' => $channel, 'videos' => $videos, 'website' => $website, 'menu' => $website->getMenu()]);
    }
}