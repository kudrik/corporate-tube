<?php

namespace App\Controller;

use App\Entity\Website;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class FavoriteVideoController extends AbstractController
{
    private $videoRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository, UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->userRepository = $userRepository;
    }

    public function addVideoToFavorite(string $extId, Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $user = $this->userRepository->findFake($website);

        $user->addVideoToFavorite($video);

        $this->userRepository->save($user);

        return $this->redirectToRoute('myfavorite');
    }

    public function removeVideoFromFavorite(string $extId, Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $user = $this->userRepository->findFake($website);

        $user->removeVideoFromFavorite($video);

        $this->userRepository->save($user);

        return $this->redirectToRoute('myfavorite');
    }

    public function favorite(Request $request)
    {
        /** @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $user = $this->userRepository->findFake($website);

        $videos = $this->videoRepository->findFavoriteByUser($user);

        return $this->render('user/myfavorite.html.twig', ['website' => $website, 'videos' => $videos, 'menu' => $website->getMenu()]);
    }
}