<?php

namespace App\Controller;

use App\Entity\Website;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    private $videoRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository, UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        /* @var Website $website */
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $isVideoFavorite = false;

        $relatedVideos = [];

        //@TODO make remote property of the vidoe lazy too
        if ($startVideo = $website->getStartVideo()) {
            $startVideo = $this->videoRepository->find($website, $startVideo->getExtId());

            $user = $this->userRepository->findFake($website);
            $isVideoFavorite = $user->isVideoFavorite($startVideo);

            if ($website->getFeature()->isRelatedVideos()) {
                $relatedVideos = $this->videoRepository->findRelatedVideos($startVideo);
            }
        }

        return $this->render(
            'index.html.twig',
            [
                'website' => $website,
                'channels' => $website->getHomeChannels(),
                'menu' => $website->getMenu(),
                'video' => $startVideo,
                'relatedVideos' => $relatedVideos,
                'isFavorite' => $isVideoFavorite
            ]);
    }
}