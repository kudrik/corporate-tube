<?php

namespace App\Controller;

use App\Entity\Rate;
use App\Entity\Website;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class RatingController extends AbstractController
{
    private $videoRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository, UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->userRepository = $userRepository;
    }

    public function create(string $extId, string $value, Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $rate = new Rate();
        $rate->setValue($value == 'like' ? 1 : -1);
        $rate->setUser(
            $this->userRepository->findFake($website)
        );

        $video->addRate($rate);

        $this->videoRepository->save($video);

        return $this->redirectToRoute('video', ['extId' => $video->getExtId()]);
    }
}