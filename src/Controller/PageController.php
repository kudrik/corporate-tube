<?php

namespace App\Controller;

use App\Entity\Page;
use App\Entity\Website;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class PageController extends AbstractController
{
    public function index(int $id, Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $page = $this->getDoctrine()->getRepository(Page::class)->findOneBy(['website' => $website, 'id' => $id]);

        return $this->render('page/index.html.twig', ['page' => $page, 'website' => $website, 'menu' => $website->getMenu()]);
    }
}