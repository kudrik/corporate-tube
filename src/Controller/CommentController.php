<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Website;
use App\Repository\UserRepository;
use App\Repository\VideoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CommentController extends AbstractController
{
    private $videoRepository;

    private $userRepository;

    public function __construct(VideoRepository $videoRepository,  UserRepository $userRepository)
    {
        $this->videoRepository = $videoRepository;
        $this->userRepository = $userRepository;
    }

    public function create(string $extId, Request $request)
    {
        $website = $this->getDoctrine()->getRepository(Website::class)->findOneByHost($request->getHost());

        $video = $this->videoRepository->find($website, $extId);

        $comment = new Comment();
        $comment->setText($request->get('text'));
        $comment->setUser(
            $this->userRepository->findFake($website)
        );

        $video->addComment($comment);

        $this->videoRepository->save($video);

        return $this->redirectToRoute('video', ['extId' => $video->getExtId()]);
    }
}