<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use App\Entity\User\FavoriteVideo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface , \Serializable
{
    const ROLES = ['ROLE_ADMIN', 'ROLE_USER'];

    use TimestampableTrait;

    private $id;

    private $website;

    private $email;

    private $roles = [];

    private $blocked = false;

    private $token;

    private $videos;

    private $comments;

    private $rates;

    private $favoriteVideos;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->favoriteVideos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): self
    {
        $result =  [];
        foreach ($roles as $role) {
            if (in_array($role, self::ROLES)) {
                $result[] = $role;
            }
        }

        $this->roles = $result;

        return $this;
    }

    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    public function setBlocked(bool $blocked): self
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * @return Collection<Video>
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function getVideosCount(): int
    {
        return $this->videos->count();
    }

    /**
     * @return Collection<Comment>
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function getCommentsCount(): int
    {
        return $this->comments->count();
    }

    public function addVideoToFavorite(Video $video): self
    {
        $favoriteVideo = new FavoriteVideo();
        $favoriteVideo->setVideo($video);
        $favoriteVideo->setUser($this);
        $favoriteVideo->setCreatedAtValue();

        $this->favoriteVideos[] = $favoriteVideo;

        return $this;
    }

    public function removeVideoFromFavorite(Video $video): self
    {
        //@TODO this does not work, doble check
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('video', $video));
        $favoriteVideo = $this->favoriteVideos->matching($criteria)->first();

        $this->favoriteVideos->removeElement($favoriteVideo);

        return $this;
    }

    /**
     * @return Collection<FavoriteVideo>
     */
    public function getFavoriteVideos(): Collection
    {
        return $this->favoriteVideos;
    }

    public function isVideoFavorite(Video $video): bool
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('video', $video));

        return boolval($this->favoriteVideos->matching($criteria)->count());
    }

    public function __toString()
    {
        return $this->getEmail();
    }

    public function getPassword()
    {
        return '';
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {

    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->email
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email
            ) = unserialize($serialized);
    }
}
