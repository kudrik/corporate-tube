<?php

namespace App\Entity\Property;


abstract class LazyProperty
{
    protected $initialized = false;

    protected $value;

    public function __toString()
    {
        if (!$this->initialized) {
            $this->doInitialize();
            $this->initialized = true;
        }

        return (string) $this->value;
    }

    abstract protected function doInitialize();
}