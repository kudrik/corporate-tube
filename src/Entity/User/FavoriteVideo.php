<?php

namespace App\Entity\User;

use App\Entity\Traits\TimestampableTrait;
use App\Entity\User;
use App\Entity\Video;

class FavoriteVideo
{
    private $id;

    private $video;

    private $user;

    use TimestampableTrait;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVideo(): ?Video
    {
        return $this->video;
    }

    public function setVideo(Video $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}