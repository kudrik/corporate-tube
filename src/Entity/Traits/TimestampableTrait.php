<?php

namespace App\Entity\Traits;

trait TimestampableTrait
{
    private $createdAt;

    private $updatedAt;

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    public function setCreatedAtValue(): self
    {
        $this->createdAt = new \DateTime();
        $this->setUpdatedAtValue();

        return $this;
    }

    public function setUpdatedAtValue(): self
    {
        $this->updatedAt = new \DateTime();

        return $this;
    }
}