<?php

namespace App\Entity;

use App\Entity\Property\LazyProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Video
{
    protected $id;

    protected $extId;

    protected $website;

    protected $user;

    protected $channels;

    protected $comments;

    protected $name;

    protected $description;

    protected $thumbnail;

    protected $thumbnails;

    protected $token;

    protected $rates;

    protected $rating = 0;

    protected $keywords;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->thumbnails = new ArrayCollection();
        $this->keywords = new ArrayCollection();
        $this->channels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExtId(): ?string
    {
        return $this->extId;
    }

    public function setExtId(string $extId): self
    {
        $this->extId = $extId;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getChannels(): Collection
    {
        return $this->channels;
    }

    public function setChannels(Collection $channels): self
    {
        $this->channels = $channels;

        return $this;
    }

    public function addChannel(Channel $channel): self
    {
        $this->channels[] = $channel;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * main thubmnail
     */
    public function getThumbnail(): ?Thumbnail
    {
        return $this->thumbnail;
    }

    public function setThumbnail(Thumbnail $thumbnail): self
    {
        $thumbnail->setVideo($this);

        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     *  list of thumbnails
     */
    public function getThumbnails(): Collection
    {
        return $this->thumbnails;
    }

    public function setThumbnails(Collection $thumbnails): self
    {
        $this->thumbnails = $thumbnails;

        return $this;
    }

    public function getPlayerId(): ?string
    {
        return $this->getWebsite()->getDataSource()->getVmproPlayerId();
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(LazyProperty $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->getWebsite()->getFeature()->isRating()) {
            throw new \Exception('Feature rating is disabled');
        }

        $rate->setVideo($this);

        $this->rates[] = $rate;

        //@TODO or recalculate it in video repo
        $this->rating = $this->rating + $rate->getValue();

        return $this;
    }

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->getWebsite()->getFeature()->isComment()) {
            throw new \Exception('Feature comment is disabled');
        }

        $comment->setVideo($this);

        $this->comments[] = $comment;

        return $this;
    }

    public function getKeywords(): Collection
    {
        return $this->keywords;
    }

    public function setKeywords(Collection $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function isStartWebsite(): bool
    {
        $sourceStartVideo = $this->getWebsite()->getSourceStartVideo();
        return $sourceStartVideo && $this->getId() === $sourceStartVideo->getId();
    }
}