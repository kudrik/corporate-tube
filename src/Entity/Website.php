<?php

namespace App\Entity;

use App\Entity\Website\HomeChannel;
use App\Entity\Website\Menu;
use App\Entity\Website\DataSource;
use App\Entity\Website\Feature;
use App\Entity\Website\IdentityProvider;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

class Website
{
    private $id;

    private $name;

    private $host;

    private $dataSource;

    private $identityProvider;

    private $feature;

    private $videos;

    private $users;

    private $channels;

    private $menu;

    private $homeChannels;

    private $pages;

    private $sourceStartVideo;

    private $sourceStartChannel;

    private $replicatedAt;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->channels = new ArrayCollection();
        $this->menu = new ArrayCollection();
        $this->homeChannels = new ArrayCollection();
        $this->pages = new ArrayCollection();
        $this->dataSource = new DataSource();
        $this->identityProvider = new IdentityProvider();
        $this->feature = new Feature();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    public function setHost(string $host): self
    {
        $this->host = $host;

        return $this;
    }

    public function getChannels(): Collection
    {
        return $this->channels;
    }

    public function addChannel(Channel $channel): self
    {
        $channel->setWebsite($this);

        $this->channels[] = $channel;

        return $this;
    }

    public function removeChannel(Channel $channel): self
    {
        $this->channels->removeElement($channel);

        return $this;
    }

    public function getDataSource(): DataSource
    {
        return $this->dataSource;
    }

    public function setDataSource(DataSource $dataSource): self
    {
        $this->dataSource = $dataSource;

        return $this;
    }

    public function getIdentityProvider(): IdentityProvider
    {
        return $this->identityProvider;
    }

    public function setIdentityProvider(IdentityProvider $identityProvider): self
    {
        $this->identityProvider = $identityProvider;

        return $this;
    }

    public function getFeature(): Feature
    {
        return $this->feature;
    }

    public function setFeature(Feature $feature): self
    {
        $this->feature = $feature;

        return $this;
    }

    public function getRootChannels(): Collection
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('parent', null));

        return $this->channels->matching($criteria);
    }

    public function getPages(): Collection
    {
        return $this->pages;
    }

    /**
     * @return Collection<Menu>
     */
    public function getMenu(): Collection
    {
        return $this->menu;
    }

    public function addMenu(Menu $menu): self
    {
        $menu->setWebsite($this);

        $this->menu[] = $menu;

        return $this;
    }

    /**
     * @return Collection<Channel>
     */
    public function getHomeChannels(): Collection
    {
        return $this->homeChannels->map(function (HomeChannel $item) { return $item->getChannel(); });
    }

    public function addHomeChannel(HomeChannel $homeChannel): self
    {
        $homeChannel->setWebsite($this);

        $this->homeChannels[] = $homeChannel;

        return $this;
    }

    public function getSourceStartVideo(): ?Video
    {
        return $this->sourceStartVideo;
    }

    public function setSourceStartVideo(?Video $startVideo): self
    {
        $this->sourceStartVideo = $startVideo;

        return $this;
    }

    public function getSourceStartChannel(): ?Channel
    {
        return $this->sourceStartChannel;
    }

    public function setSourceStartChannel(?Channel $sourceStartChannel)
    {
        $this->sourceStartChannel = $sourceStartChannel;

        return $this;
    }

    public function getStartVideo(): ?Video
    {
        if ($this->getSourceStartVideo()) {
            return $this->getSourceStartVideo();
        }

        if ($this->getSourceStartChannel()) {
            return $this->getSourceStartChannel()->getStartVideo();
        }

        //trying to find any start video
        foreach ($this->getChannels() as $channel) {
            if ($video = $channel->getStartVideo()) {
                return $video;
            }
        }

        return null;
    }

    public function getReplicatedAt(): ?\DateTime
    {
        return $this->replicatedAt;
    }

    public function setReplicatedAtValue(): self
    {
        $this->replicatedAt = new \DateTime();

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}