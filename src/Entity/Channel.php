<?php

namespace App\Entity;

use App\Entity\Traits\NestedSetsTrait;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class Channel
{
    use NestedSetsTrait;

    private $id;

    private $extId;

    private $website;

    private $name;

    private $description;

    private $videos;

    public function __construct()
    {
        $this->videos = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExtId(): ?int
    {
        return $this->extId;
    }

    public function setExtId(int $extId): self
    {
        $this->extId = $extId;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<Video>
     */
    public function getVideos(): Collection
    {
        return $this->videos;
    }

    public function setVideos(Collection $videoCollection): self
    {
        $this->videos = $videoCollection;

        return $this;
    }

    public function getStartVideo(): ?Video
    {
        return $this->getVideos()->first();
    }

    public function getNameWithLevel(): string
    {
        return str_repeat('--', $this->getLvl()) . $this->getName();
    }

    public function __toString()
    {
        return $this->name;
    }
}