<?php

namespace App\Entity\Website;

class DataSource
{
    private $vmproUser;

    private $vmproPassword;

    private $vmproId;

    private $vmproPlayerId;

    public function getVmproUser(): ?string
    {
        return $this->vmproUser;
    }

    public function setVmproUser(string $vmpro_user): self
    {
        $this->vmproUser = $vmpro_user;

        return $this;
    }

    public function getVmproPassword(): ?string
    {
        return $this->vmproPassword;
    }

    public function setVmproPassword(string $vmpro_password): self
    {
        $this->vmproPassword = $vmpro_password;

        return $this;
    }

    public function getVmproId(): ?int
    {
        return $this->vmproId;
    }

    public function setVmproId(int $vmpro_id): self
    {
        $this->vmproId = $vmpro_id;

        return $this;
    }

    public function getVmproPlayerId(): ?string
    {
        return $this->vmproPlayerId;
    }

    public function setVmproPlayerId(string $vmproPlayerId): self
    {
        $this->vmproPlayerId = $vmproPlayerId;

        return $this;
    }
}