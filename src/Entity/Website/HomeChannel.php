<?php

namespace App\Entity\Website;

use App\Entity\Traits\SortableTrait;
use App\Entity\Channel;
use App\Entity\Website;

class HomeChannel
{
    use SortableTrait;

    protected $id;

    protected $channel;

    protected $website;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(Website $website): self
    {
        $this->website = $website;

        return $this;
    }
}