<?php

namespace App\Entity\Website;

class Feature
{
    private $upload = false;

    private $comment = false;

    private $rating = false;

    private $relatedVideos = false;

    public function isUpload(): bool
    {
        return boolval($this->upload);
    }

    public function setUpload(bool $val): self
    {
        $this->upload = $val;

        return $this;
    }

    public function isComment(): bool
    {
        return boolval($this->comment);
    }

    public function setComment(bool $val): self
    {
        $this->comment = $val;

        return $this;
    }

    public function isRating(): bool
    {
        return boolval($this->rating);
    }

    public function setRating(bool $val): self
    {
        $this->rating = $val;

        return $this;
    }

    public function isRelatedVideos(): bool
    {
        return boolval($this->relatedVideos);
    }

    public function setRelatedVideos(bool $val): self
    {
        $this->relatedVideos = $val;

        return $this;
    }
}