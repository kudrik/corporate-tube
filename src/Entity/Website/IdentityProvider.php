<?php

namespace App\Entity\Website;

class IdentityProvider
{
    private $idpHint;

    private $authRealm;

    private $authResource;

    private $authUrl;

    public function getIdpHint(): ?string
    {
        return $this->idpHint;
    }

    public function setIdpHint(?string $idpHint): self
    {
        $this->idpHint = $idpHint;

        return $this;
    }

    public function getAuthRealm(): ?string
    {
        return $this->authRealm;
    }

    public function setAuthRealm(?string $authRealm): self
    {
        $this->authRealm = $authRealm;

        return $this;
    }

    public function getAuthResource(): ?string
    {
        return $this->authResource;
    }

    public function setAuthResource(?string $authResource): self
    {
        $this->authResource = $authResource;

        return $this;
    }

    public function getAuthUrl(): ?string
    {
        return $this->authUrl;
    }

    public function setAuthUrl(?string $authUrl): self
    {
        $this->authUrl = $authUrl;

        return $this;
    }
}