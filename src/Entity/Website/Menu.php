<?php

namespace App\Entity\Website;

use App\Entity\Traits\SortableTrait;
use App\Entity\Channel;
use App\Entity\Page;
use App\Entity\Website;

class Menu
{
    use SortableTrait;

    const TYPE_CHANNEL = 'channel';

    const TYPE_PAGE = 'page';

    const TYPE_LINK = 'link';

    private $id;

    private $website;

    private $type;

    private $channel;

    private $page;

    private $name;

    private $url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWebsite(): ?Website
    {
        return $this->website;
    }

    public function setWebsite(Website $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    public function setChannel(?Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getPage(): ?Page
    {
        return $this->page;
    }

    public function setPage(?Page $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getName(): ?string
    {
        $type = $this->getType();

        if ($type === self::TYPE_CHANNEL && $channel = $this->getChannel()) {
            return $channel->getName();
        }

        if ($type === self::TYPE_PAGE && $page = $this->getPage()) {
            return $page->getName();
        }

        if ($type === self::TYPE_LINK) {
            return $this->name;
        }

        return null;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url)
    {
        $this->url = $url;

        return $this;
    }

    public function getLinkParam(): array
    {
        //@TODO finish

        return [];
    }

    public function sanitize()
    {
        $type = $this->getType();

        if ($type === self::TYPE_CHANNEL) {
            $this->setPage(null);
            $this->setName(null);
            $this->setUrl(null);
        } elseif ($type === self::TYPE_PAGE) {
            $this->setChannel(null);
            $this->setName(null);
            $this->setUrl(null);
        } elseif ($type === self::TYPE_LINK) {
            $this->setChannel(null);
            $this->setPage(null);
        }

        $this->validate();
    }

    public function validate()
    {
        $type = $this->getType();

        if ($type === self::TYPE_CHANNEL && !$this->getChannel()) {
            throw new \Exception('Menu should have a channel');
        }

        if ($type === self::TYPE_PAGE && !$this->getPage()) {
            throw new \Exception('Menu should have a page');
        }

        if ($type === self::TYPE_LINK && (!$this->getName() || !$this->getUrl())) {
            throw new \Exception('Menu should have a name and a url');
        }

        if (!$type) {
            throw new \Exception('Menu should have a type');
        }
    }

    public function __toString()
    {
        return 'Menu ' . $this->getName();
    }
}