<?php

namespace App\Service;

use App\Entity\Channel;
use App\Entity\Website;
use App\Repository\VMProClient\ApiClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Replicate channels from VMPRo to ct.
 */
class DataReplication
{
    private $apiClient;

    private $entityManager;

    private $nestedSets;

    private $updatedIds = [];

    private $logger;

    public function __construct(ApiClient $apiClient,  EntityManagerInterface $entityManager, NestedSets $nestedSets, LoggerInterface $logger)
    {
        $this->apiClient = $apiClient;
        $this->entityManager = $entityManager;
        $this->nestedSets = $nestedSets;
        $this->logger = $logger;
    }

    private function replicateChannelByList(Website $website, array $channels)
    {
        foreach ($channels as $channelVmpro) {

            $this->updatedIds[] = $channelVmpro['id'];

            /** @var Channel $channel */
            $channel = $this->entityManager->getRepository(Channel::class)->findOneBy(
                ['website' => $website, 'extId' => $channelVmpro['id']]
            );

            if (!$channel) {
                $channel = new Channel();
                $channel->setWebsite($website);
            }

            $parent = $this->entityManager->getRepository(Channel::class)->findOneBy(
                ['website' => $website, 'extId' => $channelVmpro['parentId']]
            );

            $channel->setParent($parent);

            $channel->setExtId($channelVmpro['id']);
            $channel->setName($channelVmpro['name']);
            $channel->setDescription($channelVmpro['description']);

            $this->entityManager->merge($channel);
            $this->entityManager->flush();

            $this->replicateChannelByList($website, $channelVmpro['children']);
        }
    }

    private function removeNotUpdated(Website $website)
    {
        $this->entityManager->clear();

        foreach ($this->entityManager->createQueryBuilder()
                     ->select('c')
                     ->from(Channel::class,'c')
                     ->where('c.website = :website')
                     ->andWhere('c.extId NOT IN (:extIds)')
                     ->setParameter('website', $website)
                     ->setParameter('extIds',  $this->updatedIds)
                     ->getQuery()->getResult() as $channelForRemove) {

            $this->entityManager->remove($channelForRemove);
        }

        $this->entityManager->flush();
    }

    private function nestedSets(Website $website)
    {
        $this->entityManager->clear();

        $rootChannels = $this->entityManager->getRepository(Channel::class)->findBy(
            ['website' => $website, 'parent' => null],
            ['name' => 'asc']
        );

        $this->nestedSets->build($rootChannels);
    }

    public function replicateChannels(Website $website): Website
    {
        $this->entityManager->clear();

        /** @var Website $website */
        $website = $this->entityManager->getRepository(Website::class)->find($website->getId());

        $this->updatedIds = [];
        $this->replicateChannelByList(
            $website, $this->apiClient->connect($website->getDataSource())->getChannels()
        );

        $this->removeNotUpdated($website);

        $this->nestedSets($website);

        $website = $this->entityManager->getRepository(Website::class)->find($website->getId());
        $website->setReplicatedAtValue();

        $this->entityManager->flush();
        $this->entityManager->refresh($website);

        return $website;
    }
}