<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class NestedSets
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    private function tree($items, int $lft, int $level): int
    {
        $level++;

        if ($items) {

            foreach ($items as $item) {

                $lft++;

                $item->setLft($lft);

                $item->setLvl($level);

                $lft = $this->tree($item->getChildren(), $lft, $level);

                $item->setRgt($lft);
            }
        }

        return $lft + 1;
    }

    public function build(array $rootItems)
    {
        $this->tree($rootItems, 0, -1);

        $this->entityManager->flush();
    }
}