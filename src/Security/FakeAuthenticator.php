<?php

namespace App\Security;

use App\Entity\User;
use App\Entity\Website;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

/**
 * this is a FakeAuthenticator  because auth from keyclock does not implemented yet
 */
class FakeAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    private $entityManager;
    private $router;

    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    public function supports(Request $request)
    {
        return 'app_login' === $request->attributes->get('_route') && $request->getMethod() == 'POST';
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'host' => $request->get('host')
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $website = $this->entityManager->getRepository(Website::class)->findByHost($credentials['host']);

        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'website' => $website,
            'email' => $credentials['email'],
            ]);

        //try to check for superadmin
        if (!$user) {
            $user = $this->entityManager->getRepository(User::class)->findOneBy([
                'website' => null,
                'email' => $credentials['email'],
            ]);
        }

        if (!$user) {
            // fail authentication with a custom error
            throw new CustomUserMessageAuthenticationException('Email could not be found.');
        }

        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return new RedirectResponse('/');
    }

    protected function getLoginUrl()
    {
        return $this->router->generate('app_login');
    }
}